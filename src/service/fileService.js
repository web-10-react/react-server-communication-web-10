import api from "../utils/api";


export const FILE_UPLOAD = async (data) =>{
    const response = await api.post("/files/upload", data, {})
    return response.data  
}