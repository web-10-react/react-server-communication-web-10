import axios from "axios";
import React from "react";
import { useState, useEffect } from "react";
import UserCard from "../components/UserCard";
import { GET_ALL_USERS } from "../service/userService";
import { RotatingLines } from "react-loader-spinner";
const AllUsers = () => {
  const [users, setUsers] = useState([]); // original data 
  const [isLoading, setIsLoading] = useState(true);
  const [keyword, setKeyword] = useState(""); // search word
  const [filteredUser, setFilterUser] = useState([]);
  // list that we used for filtering 
  useEffect(() => {
    GET_ALL_USERS()
      .then((response) => {
        setUsers(response); // 63 users ( original data )
         setFilterUser(response); // 63 users ( change later )
        setIsLoading(false);
      })
      .catch((err) => console.log("ERROR : ", err));
  }, []);

  console.log(
    "ALL USERS : ",
    users.sort((usera, userb) =>
      usera.id > userb.id ? -1 : usera.id < userb.id ? 1 : 0
    )
  );

  let filteredList = null;
  useEffect(() => {
    if (keyword === null || keyword.trim() === "") {
      filteredList = users;
    } else {
      filteredList = users.filter((user) => user.name.toLowerCase().startsWith(keyword.toLowerCase()));
    }

    setFilterUser(filteredList);
  }, [keyword]);
  return (
    <div className="container">
      <div className="d-flex justify-content-between align-items-start mx-5 m-3">
        <h1>
          AllUsers :{" "}
          <span className="text-warning"> {filteredUser.length}</span>{" "}
        </h1>
        <input
          className="form-control w-25"
          type="text"
          placeholder="enter name to search..."
          // value={keyword}
          onChange={(e) => {
            setKeyword(e.target.value);
          }}
        />
      </div>
      {isLoading ? (
        <div className="d-flex justify-content-center">
          <RotatingLines
            strokeColor="grey"
            strokeWidth="5"
            animationDuration="0.75"
            width="96"
            visible={true}
          />
        </div>
      ) : (
        <div className="row">
          {filteredUser.length == 0 ? (
            <div>
              <h3 className="text-center"> No result </h3>
            </div>
          ) : (
            <></>
          )}

          {filteredUser.map((user) => (
            <div className="col-4 d-flex justify-content-center">
              <UserCard user={user} />
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default AllUsers;
