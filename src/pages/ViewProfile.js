import React, { useState, useEffect } from "react";
import { toast, ToastContainer } from "react-toastify";
import { CREATE_USER, GET_ONE } from "../service/userService";
import { ThreeDots } from "react-loader-spinner";
import { useParams } from "react-router-dom";
const ViewProfile = () => {
  // const [user, setUser]= useState({})
  const [isLoading, setIsLoading] = useState(false);
  const [user, setUser] = useState({});
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [avatar, setAvatar] = useState("");
  const { id } = useParams();

  useEffect(() => {
    GET_ONE(id)
      .then((response) => {
        setUser(response);
        console.log("Get one response : ", response);
        setName(response.name);
        setEmail(response.email);
        setRole(response.role);
        setPassword(response.password);
      })
      .catch((err) => console.log("ERROR IS : ", err));
  }, []);

  return (
    <div className="container ">
      <div className="wrapper d-flex bg-light rounded-3 m-4  py-5 ">
        <div
          className="image-side w-50  d-flex 
        
        justify-content-center flex-column align-items-center"
        >
          <img
            className="object-contain-fit"
            width="400px"
            src={user.avatar}
            alt="user placeholder"
          />
          <input type="file" className="form-control w-50  " />
        </div>

        <div className="input-side  mt-4  w-50 pe-5">
          <h2>User Information </h2>

          <div className="d-flex">
            <div>
              <label htmlFor="">Username </label>
              <input
                className="form-control"
                type="text"
                placeholder="Enter name "
                name=""
                id="username"
                value={name}
                disabled
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="ms-4">
              <label htmlFor="">Role </label>
              <input
                className="form-control"
                type="text"
                placeholder="admin or customer "
                name=""
                id="role"
                value={role}
                disabled
                onChange={(e) => setRole(e.target.value)}
              />
            </div>
          </div>

          <div className="d-flex">
            <div>
              <label htmlFor="">Email </label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter email "
                disabled
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>

            <div className="ms-4">
              <label htmlFor="">Password </label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter password "
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                disabled
              />
            </div>
          </div>

          <div className="d-flex mt-4 ">
            <button className="btn btn-warning" disabled>
              {" "}
              Update{" "}
            </button>
            <button className="btn btn-danger ms-5"> Cancel </button>
          </div>

          <ToastContainer />
        </div>
      </div>
    </div>
  );
};

export default ViewProfile;
